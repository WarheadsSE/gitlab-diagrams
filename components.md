Gross over-simplification of GitLab application suite & state components 

```mermaid
graph LR
  subgraph Application
    gl{{GitLab}}
    gr{{GitLab Runner}}
  end

  gl --> gr
  gr --> gl
  gl ---> PG
  gl ---> Redis
  gl ---> OS

  subgraph stateful[Stateful Data]
    PG[(PostgreSQL)]
    Redis[[Redis]]
    OS([Object Storage])
  end

  PG --> aws_RDS
  PG --> g_CSQL
  PG --> az_db
  Redis --> aws_EC
  Redis --> g_MS
  Redis --> az_ac
  OS --> aws_S3
  OS --> g_GCS
  OS --> az_ABS

  subgraph AWS
    aws_RDS[RDS / Aurora]
    aws_EC[Elasticache]
    aws_S3[S3]
  end
  
  subgraph Azure
    az_ABS[Azure Blob Storage]
    az_db[Azure Database]
    az_ac[Azure Cache]
  end

  subgraph GCP
    g_CSQL[Cloud SQL]
    g_MS[Memorystore]
    g_GCS[GCS]
  end
```
