```mermaid
sequenceDiagram
    Git Client->>+LoadBalancer: HTTPS GET /
    LoadBalancer->>+NGINX: HTTP(S) GET /
    NGINX->>+Workhorse: HTTP GET /
    Workhorse->>+Puma: HTTP GET /
    Puma->>-Workhorse: 200 OK
    Workhorse->>+Gitaly: gRPC Request

    Gitaly->>+Workhorse: Auth Verify
    Workhorse->>+Puma: Auth Confirm
    Puma->>-Workhorse: Auth OK
    Workhorse->>-Gitaly: Auth OK

    Gitaly->>-Workhorse: gRPC Response
    Workhorse->>-NGINX: HTTP Response
    NGINX->>-LoadBalancer: HTTP Response
    LoadBalancer->>-Git Client: HTTP Response

```
