##

```mermaid
sequenceDiagram
    Client->>+LoadBalancer: HTTPS GET /
    LoadBalancer->>+NGINX: HTTP(S) GET /
    loop route
        NGINX ->> NGINX: Route based on Host/ServerAlias
    end
    NGINX->>+Webservice: HTTP GET /
    Webservice->>-NGINX: HTTP 302/200 (content)
    NGINX->>-LoadBalancer: HTTP 302/200 (content)
    LoadBalancer->>-Client: HTTP 302/200 (content)
```

## Git over SSH

```mermaid
sequenceDiagram
    Client->>+LoadBalancer: SSH
    LoadBalancer->>+NGINX: SSH
    loop route
        NGINX ->> NGINX: Route based on TCP ConfigMap
    end
    NGINX->>+GitLab Shell: SSH
    GitLab Shell ->>+ Webservice: HTTP GET /API
    Webservice ->>- GitLab Shell: HTTP 401/200
    GitLab Shell ->>+ Gitaly: gRPC
    Gitaly ->>+ Webservice: HTTP GET /API
    Webservice ->>- Gitaly: HTTP 401/200
    Gitaly ->>- GitLab Shell: gRPC
    GitLab Shell ->>- NGINX: SSH
    NGINX->>-LoadBalancer: SSH
    LoadBalancer->>-Client: SSH
```

## docker login registry.gitlab.com

```mermaid
sequenceDiagram
    Client->>+LoadBalancer: GET registry.gitlab.com/
    LoadBalancer->>+NGINX: GET registry.gitlab.com/
    loop route
        NGINX ->> NGINX: Route based on Host/ServerAlias
    end
    NGINX->>+Registry: GET /
    Registry->>-NGINX: HTTP 401 (token_realm: gitlab.com)
    NGINX->>-LoadBalancer: HTTP 401
    LoadBalancer->>-Client: HTTP 401
    
    Client->>+LoadBalancer: POST gitlab.com/jwt/auth
    LoadBalancer->>+NGINX: POST /jwt/auth
    loop route
        NGINX ->> NGINX: Route based on Host/ServerAlias
    end
    NGINX->>+Webservice: POST /jwt/auth
    Webservice->>-NGINX: HTTP 401/200 (signed token)
    NGINX->>-LoadBalancer: HTTP 401/200 (signed token)
    LoadBalancer->>-Client: HTTP 401/200 (signed token)

    Client->>+LoadBalancer: GET registry.gitlab.com/
    LoadBalancer->>+NGINX: GET registry.gitlab.com/
    loop route
        NGINX ->> NGINX: Route based on Host/ServerAlias
    end
    NGINX->>+Registry: GET /
    Registry->>-NGINX: HTTP 200 (valid token)
    NGINX->>-LoadBalancer: HTTP 200 (valid token)
    LoadBalancer->>-Client: HTTP200 (valid token)
```

